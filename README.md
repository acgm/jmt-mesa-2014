# jmt-mesa-2014 #

This is the project for the extended version of the MESA
2014 paper, that will be sent to **Microsystem Technologies**.

## Recommends ##

Please, observe these rules:

1. Do not commit the pdf files of the paper.
2. If you want to commit some papers for other to read, use the "references" subdir.
3. Use the "images" subdir for pictures and images that should be used in the paper.
4. For proof of concepts, use the "src" subdir.
5. Please, use one line for every sentence. This should help git to be useful for review.
6. Please, mark every commit with:
    * [I] For commit mainly adding paragraphs or lines
    * [C] For commit mainly deleting paragraphs or lines
    * [R] For commit reviewing the language
    * [O] For other kind of commits

Other rules could be added in future, so please keep an eye on this file, if possibile.

## First submission ##

* Date: **October 27th, 2015**.
* Title: ***_A Methodological Approach to Fully Automated Highly Accelerated Life Tests_***
* Last commit: 9e06fcf1eb679303de4fbb655b55f58222f9da27
* Source download:  https://bitbucket.org/acgm/jmt-mesa-2014/get/9e06fcf1eb67.zip

## Revised manuscript ##

* Deadline: **May 7th, 2016**.
* Title: ***_A Methodological Approach to Fully Automated Highly Accelerated Life Tests_***
* Last commit:
* Source download:  

## Working paper ##

* Source download:  
 https://bitbucket.org/acgm/jmt-mesa-2014/get/master.zip

## Compilation ##

### Using ***make*** ###

```sh
make
```

### Using a shell ###

```sh
pdflatex -shell-escape template.tex
bibtex template
pdflatex template
pdflatex template
```