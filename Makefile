SRC = template.tex
TGT = template
BIB = references.bib
LIBS = tikzlibrarypaths.ortho.code.tex tikzlibrarypaths.ortho.tex
IMGPDF = img/figDBSchema.pdf
IMGTIKZ = img/figPhyArch.tex img/figSwArch1.tex img/figSwArch2.tex img/figFrameworkOrganization.tex
IMGPNG = img/figVisProg.png img/figTestCycle.png img/figGuiMonitor.png
IMGS = $(IMGPDF) $(IMGTIKZ) $(IMGPNG)
CLS = svjour3.cls svglov3.clo
BST = spbasic.bst
DIFF = submission_diff.pdf
OTHERS = CONTRIBUTORS README.md Makefile git-latexdiff submissions/MITE-D-15-00653_20151027.pdf $(DIFF)
SCRIPTS = compile.bat compile.sh
# sources to be submitted
FILE_INVENT = $(SRC) $(BIB) $(LIBS) $(IMGS) $(CLS) $(BST) $(SCRIPTS) $(DIFF)


SUBM_1 = 3aca6c4b023ab4c9db7300b6971115a1f51b1bc9

VIEWER = evince

all: $(TGT).pdf

$(TGT).pdf: $(SRC) $(CLS) $(IMGS) $(BIB) $(BST) $(LIBS)
	pdflatex -shell-escape $(TGT)
	bibtex $(TGT)
	pdflatex -shell-escape $(TGT)
	pdflatex -shell-escape $(TGT)

$(DIFF): $(TGT).pdf 
	./git-latexdiff -b  --ignore-makefile --main template.tex --latexopt -shell-escape -o $@ $(SUBM_1) HEAD
	$(VIEWER) $@ &

MITE-D-15-00653R1.zip: $(FILE_INVENT)
	zip MITE-D-15-00653R1  $(FILE_INVENT)


.PHONY: clean dist distclean pdf view
clean:
	rm -f *.aux *.auxlock *.bbl *.blg    \
	      *.brf *.dpth *.idx *.ilg *.ind \
	      *.log *.md5 $(TGT)-*.pdf *.eps \
	      *~
distclean: clean
	rm -f $(TGT).pdf $(DIFF)

dist: $(TGT).pdf $(SRC) $(CLS) $(IMGS) $(BIB) $(BST) $(LIBS) $(OTHERS) $(DIFF)
	tar cvzf MITE-D-15-00653.tar.gz $?

pdf: $(TGT).pdf

view: all
	$(VIEVER) $(TGT).pdf

